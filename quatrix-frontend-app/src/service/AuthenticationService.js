import axios from 'axios'

const API_URL = 'https://quatrix-backend-api.herokuapp.com'

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

class AuthenticationService {

    executeJwtAuthenticationService(phone, password) {
        console.log(phone);
        return axios.post(`${API_URL}/authenticate`, {
            phone,
            password
        })
    }

    registerSuccessfulLogin(phone, password) {
       
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, phone)
        this.setupAxiosInterceptors(this.createBasicAuthToken(phone, password))
    }

    registerSuccessfulLoginForJwt(phone, token) {
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, phone)
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }

    createJWTToken(token) {
        return 'Bearer ' + token
    }


    logout() {
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return false
        return true
    }

    getLoggedInUsername() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return ''
        return user
    }

    setupAxiosInterceptors(token) {
        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = token
                }
                return config
            }
        )
    }
}

export default new AuthenticationService()