import React, { Component } from 'react';
import TaskListComponent from './TaskListComponent';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import LoginComponent from './LoginComponent';
import LogoutComponent from './LogoutComponent';
import MenuComponent from './MenuComponent';
import AuthenticateRoute from './AuthenticateRoute';

class TaskApp extends Component {


    render() {
        return (
            <>
                <Router>
                    <>
                        <MenuComponent />
                        <Switch>
                            <Route path="/" exact component={LoginComponent} />
                            <Route path="/login" exact component={LoginComponent} />
                            <AuthenticateRoute path="/logout" exact component={LogoutComponent} />
                            <AuthenticateRoute path="/task/assigned" exact component={TaskListComponent} />
                        </Switch>
                    </>
                </Router>
            </>
        )
    }
}

export default TaskApp