import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import {  Navbar, Nav, } from 'react-bootstrap';
import AuthenticationService from '../service/AuthenticationService';


class MenuComponent extends Component {

    render() {
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();

        return (
            <div className="container">
            <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Quatrix Test Application</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/task/assigned">Home</Nav.Link>
                
                
              </Nav>
              <ul className="navbar-nav navbar-collapse justify-content-end">
                        {!isUserLoggedIn && <li><Link className="nav-link" to="/login">Login</Link></li>}
                        {isUserLoggedIn && <li><Link className="nav-link" to="/logout" onClick={AuthenticationService.logout}>Logout</Link></li>}
                    </ul>
            </Navbar.Collapse>
          </Navbar>
          </div>
        )
    }
}

export default withRouter(MenuComponent)