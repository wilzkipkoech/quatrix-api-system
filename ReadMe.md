# Spring Boot  and Reactjs Full Stack With Jwt Authetication Features
The code base for the Backend API for the given test by Quatrix


### spring Boot and JWT Impelementation


### Heroku

The backend application is up and running on Heroku server on the Link [ https://quatrix-backend-api.herokuapp.com/]



### Registering a new user

https://quatrix-backend-api.herokuapp.com/user

   ```
    {
    "phone": "0722123123",
    "password": "123456"
   
}
```

### Log in url and for generating a json web token
 https://quatrix-backend-api.herokuapp.com/authenticate
 
 

   ```
    {
    "phone": "0722123123",
    "password": "123456"
   
}
```

### create a new Resource
https://quatrix-backend-api.herokuapp.com/post/task

  ```
   {
        "id": 1,
        "customer_first_name": "Stephen ",
        "personnel_first_name": "kosgei",
        "personnel_other_name": "nancy",
        "customer_last_name": "jerono",
        "customer_phone": "0717890426",
        "agentId": 123,
        "assigned": "yes",
        "in_progress": "no",
        "completed": null,
        "deferred": null,
        "status": null,
        "location": "yess",
        "gender": "male",
        "age": "24",
        "access_code": null,
        "mpesa": null,
        "autoplay": null,
        "comments": "urgentnly",
        "registration": null
    }
   
```


### Retrieve all the resources
https://quatrix-backend-api.herokuapp.com/task/assigned



### Local set up

The code base contains upto date dependencies no need of adding any dependency


### End Points on Local set up

 Registering a new user
     [localhost:8007/user]

Log in url and for generating a json web token      

      [localhost:8007/authenticate]


### Reactjs Front End App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Heroku

The backend application is up and running on Heroku server on the Link [ https://quatrix-frontendapp.herokuapp.com/task/assigned]\

### Log in credentials

  username  0722123123
  password  123456



# Local machine
>

